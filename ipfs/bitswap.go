package ipfs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	bitswap "github.com/ipfs/go-bitswap"
	"github.com/ipfs/go-bitswap/decision"
	"github.com/libp2p/go-libp2p/core/peer"
	log "github.com/sirupsen/logrus"
)

func init() {
	value := os.Getenv("LOG")
	switch value {
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
		break
	default:
		log.SetLevel(log.InfoLevel)
	}
	log.SetOutput(os.Stdout)
}

type Bitswap struct {
	IPFSSwap  *bitswap.Bitswap
	Analysis  map[string]*decision.Receipt
	PeersAddr map[string]IpLocation
}

func NewBitSwap(node *IpfsNode) (*Bitswap, error) {
	swap, ok := node.Ipfs.Exchange.(*bitswap.Bitswap)
	if !ok {
		return nil, fmt.Errorf("", ok)
	}
	return &Bitswap{swap, make(map[string]*decision.Receipt), make(map[string]IpLocation)}, nil
}

func (swap *Bitswap) MakeAnalysis() error {
	status, err := swap.IPFSSwap.Stat()
	if err != nil {
		return err
	}

	peersList := status.Peers

	mapResult := make(map[string]*decision.Receipt)
	for _, connPeer := range peersList {
		id := connPeer
		partner, _ := peer.Decode(string(id))
		result := swap.IPFSSwap.LedgerForPeer(partner)
		if result.Recv > 0 || result.Sent > 0 {
			mapResult[id] = result
			log.Debug("ID peer: ", id)
			log.Debug("Received from peer ", result.Recv)
			log.Debug("Sent to peer ", result.Sent)
			log.Debug("Exchange betwen us ", result.Exchanged)
		}
	}
	swap.Analysis = mapResult
	return nil
}

func (swap Bitswap) DumpStatistic() error {
	contributorsJson, err := json.Marshal(swap.Analysis)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(".spygo/contributors.json", contributorsJson, 0644)
	if err != nil {
		return err
	}

	lookupPeersJson, err := json.Marshal(swap.PeersAddr)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(".spygo/lookup.json", lookupPeersJson, 0644)
}
