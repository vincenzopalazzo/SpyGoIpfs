package ipfs

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"math"
	"net"
	"os"
	"strconv"
	"strings"

	ipld "github.com/ipfs/go-ipld-format"
	"github.com/ipfs/interface-go-ipfs-core/path"
	"github.com/mitchellh/mapstructure"
)

type Data struct {
	Type string  `json:"type"`
	Mins []int64 `json:"mins"`
}

type LocationData struct {
	Min  int64         `json:"min"`
	Data []interface{} `json:"data"`
}

type DataLeft struct {
	Type string         `json:"type"`
	Data []LocationData `json:"data"` // TODO this is a strange Object
}

func init() {
	value := os.Getenv("LOG")
	switch value {
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
		break
	default:
		log.SetLevel(log.InfoLevel)
	}
	log.SetOutput(os.Stdout)
}

func GenericParsing(node *IpfsNode, cid string, toFind int64) (*Location, error) {
	location, err := callObjData(node, cid)
	if err != nil {
		log.Error("Error during the call ipfs object data <cid>")
		return nil, err
	}
	tipe := location["type"]
	switch tipe {
	case "Node":
		return parsingNode(node, cid, toFind)
	case "Leaf":
		return parsingLeaf(node, cid, toFind)
	}
	// This should never happen
	return nil, errors.New("Type Object not recognized")
}

func parsingNode(node *IpfsNode, cid string, toFind int64) (*Location, error) {
	log.Debug("IP to find: ", toFind)
	childIndex := 0
	genericLocation, err := callObjData(node, cid)
	if err != nil {
		log.Error("Error during the call ipfs object data <cid>")
		return nil, err
	}
	var data Data
	err = mapstructure.Decode(genericLocation, &data)
	if err != nil {
		log.Error("parsingNode: Error during decoding the generic map into a struct")
		return nil, err
	}

	log.Info("Size Mins in the json", len(data.Mins))
	for len(data.Mins) < childIndex && data.Mins[childIndex] <= toFind {
		childIndex++
	}
	// Make another IPFS request to get the links of the object,
	// this is possible with the command IPFS
	// https://gitlab.com/vincenzopalazzo/SpyGoIpfs/-/issues/6#note_547437753
	getRes, err := callObjGet(node, cid)
	if err != nil {
		log.Error("Error during the call ipfs object get <cid>")
		return nil, err
	}

	next := getRes.Links()[childIndex]
	log.Info("Size links", len(getRes.Links()))
	return recLookup(node, next.Cid.String(), toFind)
}

func parsingLeaf(node *IpfsNode, cid string, toFind int64) (*Location, error) {
	childIndex := 0
	genericLocation, err := callObjData(node, cid)
	if err != nil {
		return nil, err
	}

	dataType := genericLocation["data"]
	// Very back hack but the it is a dynamic JSON and we need to check
	// the type of the obj to use the api object
	switch dataType.(type) {
	case float64:
		log.Error("parsingLeaf: The information are not present in the database")
		return nil, nil
	case []interface{}:
		var data []interface{}
		err = mapstructure.Decode(genericLocation["data"], &data)
		if err != nil {
			log.Error("parsingLeaf: Error during decoding the generic map into a struct")
			return nil, err
		}
		correctList := make([]LocationData, 0)
		for _, elem := range data {
			var obj map[string]interface{}
			err = mapstructure.Decode(elem, &obj)
			if err != nil {
				log.Error("line 121 in for elem := range data: Error during decoding the generic map into a struct")
				return nil, err
			}
			switch obj["data"].(type) {
			case []interface{}:
				conv := int64(math.Round((obj["min"].(float64))))
				log.Debug("Analyzing the type field: ", conv)
				log.Debug("Analyzing the data field: ", obj["data"])
				correctList = append(correctList, LocationData{conv, obj["data"].([]interface{})})
			}
		}
		for len(correctList) < childIndex && correctList[childIndex].Min <= toFind {
			childIndex++
		}
		next := correctList[childIndex].Data
		log.Debug("The node information are enough to find the city")
		return &Location{next[0].(string), next[1].(string),
			next[2].(string), next[3].(string), next[4].(string), next[5].(float64),
			next[6].(float64), "Earth"}, nil
	}
	log.Error("The node information are NOT enough to find the city")
	return nil, errors.New("Informations not found")
}

func IsLocalIp(toFind string) (bool, error) {
	tokens := strings.Split(toFind, ".")
	switch tokens[0] {
	case "10":
		return true, nil
	case "127":
		return true, nil
	case "192":
		if tokens[1] == "168" {
			return true, nil
		}
		return false, nil
	case "172":
		// TODO: Improve error handling
		numOne, _ := strconv.Atoi(tokens[1])
		numTwo, _ := strconv.Atoi(tokens[2])
		if numOne >= 16 && numTwo <= 31 {
			return true, nil
		}
		return false, nil
	default:
		return false, nil
	}
}

// See https://gist.github.com/ammario/649d4c0da650162efd404af23e25b86b
func Ip2Long(ipStr string) uint32 {
	ip := net.ParseIP(ipStr)
	if ip == nil {
		return 0
	}
	ip = ip.To4()
	return binary.BigEndian.Uint32(ip)
}

func callObjGet(node *IpfsNode, cid string) (ipld.Node, error) {
	createPath := path.New(cid)
	nodeRes, err := node.Node.Object().Get(node.Context, createPath)
	if err != nil {
		return nil, err
	}
	return nodeRes, nil
}

func callObjData(node *IpfsNode, cid string) (map[string]interface{}, error) {
	createPath := path.New(cid)
	reader, err := node.Node.Object().Data(node.Context, createPath)
	if err != nil {
		return nil, err
	}
	toEncode, err := ioutil.ReadAll(reader)
	log.Debug("Cid resolved with ", cid, " data: ", string(toEncode))
	var result map[string]interface{}
	if err := json.Unmarshal(toEncode, &result); err != nil {
		return nil, err
	}
	return result, nil
}

/**

if err != nil {
	return nil, err
}
log.Debug("Response after call to object data: ", string(toEncode))
var database LocationInfo
if err := json.Unmarshal(toEncode, &database); err != nil {
	return nil, err
}
*/
