package ipfs

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	files "github.com/ipfs/go-ipfs-files"
	"github.com/schollz/progressbar/v3"
)

// WriteTo writes the given node to the local filesystem at fpath.
// Customizzation code from the https://github.com/ipfs/kubo-files/blob/master/filewriter.go Thanks :-)
func WriteTo(nd files.Node, fpath string, multifile bool) error {
	switch nd := nd.(type) {
	case *files.Symlink:
		return os.Symlink(nd.Target, fpath)
	case files.File:
		f, err := os.Create(fpath)
		defer f.Close()
		if err != nil {
			return err
		}
		if !multifile {
			size, _ := nd.Size()
			bar := progressbar.DefaultBytes(
				size,
				"Pull File",
			)
			_, err = io.Copy(io.MultiWriter(f, bar), nd)
			if err != nil {
				return err
			}
		} else {
			_, err = io.Copy(f, nd)
			if err != nil {
				return err
			}
		}
		return nil
	case files.Directory:
		err := os.Mkdir(fpath, 0777)
		if err != nil {
			return err
		}
		counter := 0
		entries := nd.Entries()
		for entries.Next() {
			counter++
		}
		entries = nd.Entries()
		bar := progressbar.NewOptions(counter,
			progressbar.OptionEnableColorCodes(true),
			progressbar.OptionShowBytes(true),
			progressbar.OptionSetDescription("[white]Pull Directory[white]"),
			progressbar.OptionSetTheme(progressbar.Theme{
				Saucer:        "[green]=[reset]",
				SaucerHead:    "[green]>[reset]",
				SaucerPadding: " ",
				BarStart:      "[",
				BarEnd:        "]",
			}))
		for entries.Next() {
			bar.Add(1)
			child := filepath.Join(fpath, entries.Name())
			if err := WriteTo(entries.Node(), child, true); err != nil {
				return err
			}
		}
		bar.Finish()
		return entries.Err()
	default:
		return fmt.Errorf("file type %T at %q is not supported", nd, fpath)
	}
}
