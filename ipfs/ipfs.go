package ipfs

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/ipfs/kubo/core/node/libp2p"
	"github.com/multiformats/go-multiaddr"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/vincenzopalazzo/SpyGoIpfs/utils"

	ipfsconfig "github.com/ipfs/kubo/config"
	files "github.com/ipfs/go-ipfs-files"
	icore "github.com/ipfs/interface-go-ipfs-core"
	icorepath "github.com/ipfs/interface-go-ipfs-core/path"

	"github.com/ipfs/kubo/core"
	"github.com/ipfs/kubo/core/coreapi"
	"github.com/ipfs/kubo/plugin/loader" // This package is needed so that all the preloaded plugins are loaded automatically
	"github.com/ipfs/kubo/repo/fsrepo"
	"github.com/libp2p/go-libp2p/core/peer"

	"github.com/dustin/go-humanize"
	log "github.com/sirupsen/logrus"
)

type IpfsNode struct {
	Node    icore.CoreAPI
	Ipfs    *core.IpfsNode
	Config  utils.Config
	Context context.Context
}

func init() {
	value := os.Getenv("LOG")
	switch value {
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
		break
	default:
		log.SetLevel(log.InfoLevel)
	}
	log.SetOutput(os.Stdout)
}

func setupPlugins(externalPluginsPath string) error {
	// Load any external plugins if available on externalPluginsPath
	plugins, err := loader.NewPluginLoader(filepath.Join(externalPluginsPath, "plugins"))
	if err != nil {
		return fmt.Errorf("error loading plugins: %s", err)
	}

	// Load preloaded and external plugins
	if err := plugins.Initialize(); err != nil {
		return fmt.Errorf("error initializing plugins: %s", err)
	}

	if err := plugins.Inject(); err != nil {
		return fmt.Errorf("error initializing plugins: %s", err)
	}
	log.Debug("IPFS plugin initialized")
	return nil
}

func initNode(nodePath string) (string, error) {
	// init plugins
	//https://github.com/ipfs/kubo/issues/6416
	if err := setupPlugins(""); err != nil {
		return "", err
	}

	if _, err := os.Stat(nodePath); os.IsNotExist(err) {
		err = os.Mkdir(nodePath, 0755)
		if err != nil {
			return "", err
		}
		log.Info("Work dir create in ", nodePath)
	}

	nodePath += "/node"

	if _, err := os.Stat(nodePath); os.IsNotExist(err) {
		err = os.Mkdir(nodePath, 0755)
		if err != nil {
			return "", err
		}
		log.Info("Node dir create in ", nodePath)
	}

	// make a key of 2048 bits
	configNode, err := ipfsconfig.Init(ioutil.Discard, 2048)
	if err != nil {
		return "", err
	}

	err = fsrepo.Init(nodePath, configNode)
	if err != nil {
		return "", err
	}
	log.Debug("IPFS inizialized")
	return nodePath, nil
}

func CreateIpfsNode(context context.Context, localDirStr string) (*IpfsNode, error) {
	repoPath, err := initNode(localDirStr)
	if err != nil {
		return nil, err
	}
	localDir, err := fsrepo.Open(repoPath)
	if err != nil {
		return nil, err
	}

	nodeOptions := &core.BuildCfg{
		Online:  true,
		Routing: libp2p.DHTOption, // This option sets the node to be a full DHT node (both fetching and storing DHT Records)
		//Routing: libp2p.DHTClientOption, // This option sets the node to be a client DHT node (only fetching records)
		Repo: localDir,
	}

	// Init
	node, err := core.NewNode(context, nodeOptions)
	if err != nil {
		return nil, err
	}
	log.Debug("IPFS node created")
	core, err := coreapi.NewCoreAPI(node)
	if err != nil {
		return nil, err
	}
	log.Debug("IPFS node api created")
	config, err := utils.LoadConfig("")
	if err != nil {
		return nil, err
	}
	log.Debug("Peers address loaded ", config.Peers)
	nodeCli := IpfsNode{Node: core, Ipfs: node, Config: *config, Context: context}

	err = nodeCli.connectNode()
	if err != nil {
		return nil, err
	}
	log.Debug("Finish connection to node")
	return &nodeCli, nil
}

func (node IpfsNode) connectNode() error {
	peers := node.Config.Peers
	// Create the peers (IPFS context)
	peersInfo := make(map[peer.ID]*peer.AddrInfo, len(peers))

	for _, address := range peers {
		addr, err := multiaddr.NewMultiaddr(address)
		if err != nil {
			return err
		}
		peerInfo2, err := peer.AddrInfoFromP2pAddr(addr)
		if err != nil {
			return err
		}

		peerIn, ok := peersInfo[peerInfo2.ID]
		if !ok {
			peerIn = &peer.AddrInfo{ID: peerInfo2.ID}
			peersInfo[peerIn.ID] = peerIn
		}
		peerIn.Addrs = append(peerIn.Addrs, peerInfo2.Addrs...)
	}

	var courutinesWait sync.WaitGroup
	courutinesWait.Add(len(peers))
	for _, peersInfo := range peersInfo {
		go node.connectToNode(peersInfo, &courutinesWait)
	}
	courutinesWait.Wait()
	return nil
}

func (node IpfsNode) connectToNode(peerInfo *peer.AddrInfo, corutine *sync.WaitGroup) {
	defer corutine.Done()
	log.Debug("Start connection to ", peerInfo.ID)
	err := node.Node.Swarm().Connect(node.Context, *peerInfo)
	if err != nil {
		log.Debug("failed to connect to", peerInfo.ID, " error received is ", err)
		return
	}
	log.Debug("Connection to peer ", peerInfo.ID)
}

func (node IpfsNode) GetContent(cid string) error {
	// log.Debug("Node result ", node)
	completeCid := ".spygo/" + cid
	resolve := node.Resolve(cid)
	log.Info("Resolve the query on ", completeCid)
	fileReader, err := node.GentWithUnix(resolve)
	if err != nil {
		return err
	}
	log.Info("Saving File ...")

	err = WriteTo(*fileReader, completeCid, false)
	if err != nil {
		return err
	}
	log.Info("Completed, the file is ready on your system")
	return node.dumpInfo(cid, resolve)
}

func (node IpfsNode) Resolve(hash string) icorepath.Path {
	return icorepath.New(hash)
}

func (node IpfsNode) GentWithUnix(path icorepath.Path) (*files.Node, error) {
	fileReader, err := node.Node.Unixfs().Get(node.Context, path)
	log.Info("Received file from network")
	if err != nil {
		return nil, err
	}
	return &fileReader, nil
}

func (node IpfsNode) dumpInfo(cid string, cidPath icorepath.Path) error {
	infoJson := make(map[string]string)
	infoCid, err := node.Node.Object().Stat(node.Context, cidPath)
	if err != nil {
		return err
	}
	infoJson[cid] = humanize.Bytes(uint64(infoCid.CumulativeSize))
	saveInfo, err := json.Marshal(infoJson)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(".spygo/info.json", saveInfo, 0644)
}
