package ipfs

import (
	"encoding/json"
	"github.com/libp2p/go-libp2p/core/peer"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"regexp"
	"strings"
)

func init() {
	value := os.Getenv("LOG")
	switch value {
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
		break
	default:
		log.SetLevel(log.InfoLevel)
	}
	log.SetOutput(os.Stdout)
}

type IpLocation struct {
	City      string `json:"city"`
	Continent string `json:"continent"`
	Country   string `json:"country"`
	Region    string `json:"region"`
	Protocol  string //TODO: give the name to json in lower case
	Port      string
}

func GeolocalizePeers(node *IpfsNode, bitswap *Bitswap) error {
	knownAddrs, err := node.Node.Swarm().KnownAddrs(node.Context)
	if err != nil {
		return err
	}
	regexFindIp := regexp.MustCompile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}`)
	if err != nil {
		return err
	}

	for peerIdStr := range bitswap.Analysis {
		key, err := peer.Decode(peerIdStr)
		if err != nil {
			return err
		}
		peerAddresses := knownAddrs[key]
		var peerAddress string
		log.Info("ID: ", peerIdStr)
		for _, address := range peerAddresses {
			peerAddress = address.String()
			log.Debug("Node addr is: ", peerAddress)
			listIp := regexFindIp.FindAllString(peerAddress, -1)
			if len(listIp) == 0 {
				continue
			}
			ip := listIp[0]
			result, err := makeAPiCall(ip)
			if err != nil {
				return err
			}
			if result != nil {
				tokens := strings.Split(peerAddress, "/")
				parseProtocol(result, tokens)
				log.Info("Peer with protocol: ", result.Protocol, " on port ", result.Port)
				bitswap.PeersAddr[peerIdStr] = *result
				log.Info("Country node is: ", result.Country)
			}
		}
	}
	return nil
}

func parseProtocol(ipInfo *IpLocation, tokens []string) {
	switch tokens[len(tokens)-1] {
	case "quic":
		ipInfo.Protocol = tokens[len(tokens)-1]
		ipInfo.Port = tokens[len(tokens)-2]
	case "ws":
		ipInfo.Protocol = tokens[len(tokens)-1]
		ipInfo.Port = tokens[len(tokens)-2]
	default:
		ipInfo.Protocol = tokens[len(tokens)-2]
		ipInfo.Port = tokens[len(tokens)-1]
	}
}

func makeAPiCall(ip string) (*IpLocation, error) {
	local, err := IsLocalIp(ip)
	if local {
		return nil, err
	}
	url := "http://extreme-ip-lookup.com/json/" + ip
	log.Debug("Request to: ", url)
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	/*
		TODO the craft implementation of the go-geoipfs is buggy
		log.Debug("TEST MODE")
		location, err := LiteLookUp(node, ip)
		if err != nil {
			log.Error("Error received during making the analysis")
			log.Error(err)
			//return err
			continue
		}
		if location == nil {
			log.Info("Local ip: ", ip)
			continue
		}
		log.Info("Location found with go-geoipfs: ", location)
	*/
	var result = new(IpLocation)
	err = json.NewDecoder(response.Body).Decode(result)
	if err != nil {
		return nil, err
	}

	if result.City != "" {
		return result, nil
	}
	return nil, nil
}
