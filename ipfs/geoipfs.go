package ipfs

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"strconv"
)

var DB_LOCATION = "Qmbt1YbZAhMoqo7r1t6Y5EJrYGVRgcaisNAZhLeJY6ehfg"

type Link struct {
	Name string `json:"Name"`
	Hash string `json:"Hash"`
	Size int64  `json:"Size"`
}

type Result struct {
	Min int64 `json:"min"`
}

type LocationInfo struct {
	Links []Link `json:"Links"`
	Data  Data   `json:"Data"`
}

type Location struct {
	CountryName string
	CountryCode string
	RegionCode  string
	City        string
	PostalCode  string
	latitude    float64
	longitude   float64
	planet      string
}

func (location Location) String() string {
	return fmt.Sprintf("%s, %s", location.CountryName, location.City)
}

func init() {
	value := os.Getenv("LOG")
	switch value {
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
		break
	default:
		log.SetLevel(log.InfoLevel)
	}
	log.SetOutput(os.Stdout)
}

func LiteLookUp(node *IpfsNode, toFind string) (*Location, error) {
	isLocal, err := IsLocalIp(toFind)
	if err != nil {
		log.Error("Error during check is the ip is a local ip")
		return nil, err
	}
	if isLocal {
		log.Debug("The ip: ", toFind, " is a local ip")
		return nil, nil
	}
	ipToLong := Ip2Long(toFind)
	log.Debug("Conversion ip ", toFind, " into long number ", ipToLong)
	return recLookup(node, DB_LOCATION, int64(ipToLong))
}

func recLookup(node *IpfsNode, cidDb string, toFind int64) (*Location, error) {
	toString := strconv.FormatInt(toFind, 10)
	log.Debug("Conversion IP to string ", toString)
	log.Debug("CID to Resolving ", cidDb)
	return GenericParsing(node, cidDb, toFind)
}
