# SpyGoIpfs

<div align="center">
<img src="https://i.ibb.co/Q6grmLy/go-icon.png" width="240" height="240" align="center"/>
</div>

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/vincenzopalazzo/SpyGoIpfs/master?style=flat-square)
![Docker Image Version (latest by date)](https://img.shields.io/docker/v/vincenzopalazzo/spygoipfs?style=flat-square)
![Docker Pulls](https://img.shields.io/docker/pulls/vincenzopalazzo/spygoipfs?style=flat-square)

SpyGoIPFS is a lite client developer with Go language, it is developer with the Go language API and with the example library at the [following link](https://github.com/ipfs/kubo/tree/master/docs/examples/go-ipfs-as-a-library). It is full based in the [go-ipfs](https://github.com/ipfs/kubo) implementation, and it used all most all interface available for the Go language.

The library used are available below

- [interface-go-ipfs-core](https://github.com/ipfs/interface-go-ipfs-core): This library is used to work with the path resolution and all the interface of the node
- [go-multiaddr](https://github.com/multiformats/go-multiaddr): It is used to bootstrap the node with the peers available on `peers.json` file.
- [go-ipfs](https://github.com/ipfs/kubo): Some code imported from go-ipfs to make the configuration of the network
- [go-libp2p-core](https://github.com/libp2p/go-libp2p/core): This library is used to managed the connection to the nodes.
- [go-bitswap](https://github.com/ipfs/go-bitswap): The library to work with the bitswap commands.
- [go-ipfs-files](https://github.com/ipfs/kubo-files): It is used to convert the binary data into the correct form (directory, file, ecc)

## Table of Content

- [How it works]()
- [How run it]()
- [Docker]()
- [Paper]()
- [License]()

## How it works

SpyGoIPFS can be compiler with the `go build .` command or with the command `make`  and it produces an executable for
the user machine compatible with the architecture called *SpyGoIPFS*, and it has some rules that are summarized in the next sections.\\
After the first run, the application creates a local directory called `.spygo` that contains all the data produced from the application,
and this directory is created in the same position as the runnable program.\\

## How run it

SpyGoIPFS give the possibility to download one content per time in the directory `.spygo`.
During the download the application print some information on the console about the status of the download, and at the end of it; the program
will end. The log of the application is managed by the environment, to enable the debug mode is possible to export the propriety LOG="debug".

```bash
./SpyGoIPFS -d QmdmQXB2mzChmMeKY47C43LxUdg1NDJ5MWcKMKxDu7RgQm
```

To start the analysis of the network during the download of a file the user needs to specify the option {\bf -a}, and the software will produce
three different json in the _.spygo_ called _info.json_, _lookup.json_ and _contributors.json_.

These files contain the following information

- _info.json_: It contains the information's relative to the node, and the download of the file;
- _lookup.json_: It contains the information's relative to the peers, it includes all info relative to the peers, included geolocalization;
- _contributors.json_: It contains all information on how much the peers has contributed to the download of the file.

```bash
./SpyGoIPFS -d QmdmQXB2mzChmMeKY47C43LxUdg1NDJ5MWcKMKxDu7RgQm -a
```

## Docker

The project can be run with the docker compose with the following command
```bash
>> docker-compose up
>> docker-compose run -T spygoipfs bash
>> ./SpyGoIPFS --help
usage: SpyGoIpfs [-h|--help] [-v|--version] [-d|--download "<value>"]
                 [-a|--analysis] [-p|--peer "<value>"]

                 SpyGoIpfs is an academic application to work with a IPFS.

Arguments:

  -h  --help      Print help information
  -v  --version   Print version application. Default: false
  -d  --download  Download all the reference from the IPFS url
  -a  --analysis  Make available on the analysis about the download. Default:
                  false
  -p  --peer      Connect to a specific peers (particular util for the first
                  used). Default: 
```

## Descriptions

Some additional description are available at the following link

- [Paper](https://gitlab.com/vincenzopalazzo/competitiveprogrammingreport/-/tree/master/p2p/midterm1)
- [Jupiter Notebook](https://gitlab.com/vincenzopalazzo/SpyGoIpfs/-/tree/master/report)

### License

<div align="center">
  <img src="https://opensource.org/files/osi_keyhole_300X300_90ppi_0.png" width="150" height="150"/>
</div>

SpyGoIPFS a lite node for IPFS developed with University of Pisa for
courses Peer to Peer and Blockchains.

Copyright (C) 2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
and all professors of the Peer to Peer and Blockchains class

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.