package main

import (
	"context"
	"fmt"
	"os"

	"github.com/akamensky/argparse"

	log "github.com/sirupsen/logrus"
	"gitlab.com/vincenzopalazzo/SpyGoIpfs/ipfs"
)

func init() {
	value := os.Getenv("LOG")
	if value == "" {
		value = "INFO"
	}
	switch value {
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
		break
	default:
		log.SetLevel(log.InfoLevel)
	}
	log.SetOutput(os.Stdout)
}

func configCommandLine() (*argparse.Parser, *bool, *string, *bool, *string) {
	parser := argparse.NewParser("SpyGoIpfs",
		"SpyGoIpfs is an academic application to work with a IPFS.")
	version := parser.Flag("v", "version",
		&argparse.Options{Required: false, Default: false,
			Help: "Print version application"})
	urlFlag := parser.String("d", "download",
		&argparse.Options{Required: false,
			Help: "Download all the reference from the IPFS url"})
	analysisFlag := parser.Flag("a", "analysis",
		&argparse.Options{Required: false, Default: false,
			Help: "Make available on the analysis about the download"})
	peersFlag := parser.String("p", "peer",
		&argparse.Options{Required: false, Default: "",
			Help: "Connect to a specific peers (particular util for the first used)"})
	return parser, version, urlFlag, analysisFlag, peersFlag
}

func main() {

	parser, version, url, analysisFlag, peers := configCommandLine()

	err := parser.Parse(os.Args)

	if err != nil {
		fmt.Println(err)
		return
	}

	log.Debug("Version ", *version)
	log.Debug("CID ", *url)
	if *version {
		fmt.Println("SpyGoIpfs v0.0.2")
		return
	}

	if len(*peers) > 0 {
		fmt.Println("Function unsupported in the version v0.0.2")
		return
	}

	if len(*peers) == 0 {
		fmt.Println("\nPlease the download command is required.\nTry ./SpyGoIPFS -d QmbsZEvJE8EU51HCUHQg2aem9JNFmFHdva3tGVYutdCXHp")
		fmt.Println("or ./SpyGoIPFS --help for more information\n")
		return
	}

	// Get app contenxt
	context, cancel := context.WithCancel(context.Background())
	defer cancel()

	node, err := ipfs.CreateIpfsNode(context, ".spygo")
	log.Info("Node ready ...")
	if err != nil {
		fmt.Println(err)
		return
	}

	err = node.GetContent(*url)
	if err != nil {
		fmt.Println(err)
		return
	}

	if *analysisFlag {
		log.Info("Analisis mode enabled")

		// From: https://github.com/ipfs/kubo/blob/a8c7980721909f9cd16c35d699bc481574512c38/core/commands/bitswap.go#L56
		swap, err := ipfs.NewBitSwap(node)
		if err != nil {
			fmt.Println(err)
			return
		}
		log.Info("Init Bitswam module")
		err = swap.MakeAnalysis()
		if err != nil {
			fmt.Println(err)
			return
		}

		err = ipfs.GeolocalizePeers(node, swap)
		log.Debug("geoalication peers")
		if err != nil {
			fmt.Println(err)
			return
		}

		err = swap.DumpStatistic()
		if err != nil {
			fmt.Println(err)
			return
		}

	}
}
