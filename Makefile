PROJECT_NAME=SpyGoIPFS

CXX=go
ZIP=tar
ZIP_FLAG=cfzv
VERSION=v0.0.2

default:
	$(CXX) get
	$(CXX) build -o $(PROJECT_NAME)

clean:
	rm -rf .spygo $(PROJECT_NAME)*

check:
	$(CXX) test gitlab.com/vincenzopalazzo/SpyGoIpfs/tests

package:
	rm -rf dist
	mkdir dist
	cp peers.json dist
	env GOOS=linux GOARCH=386 $(CXX) build -o dist/$(PROJECT_NAME)-linux-386.out
	env GOOS=linux GOARCH=amd64 $(CXX) build -o dist/$(PROJECT_NAME)-linux-amd64.out
	env GOOS=linux GOARCH=arm $(CXX) build -o dist/$(PROJECT_NAME)-linux-arm.out
	env GOOS=linux GOARCH=arm64 $(CXX) build -o dist/$(PROJECT_NAME)-linux-arm64.out
	env GOOS=windows GOARCH=386 $(CXX) build -o dist/$(PROJECT_NAME)-windows-386.exe
	env GOOS=windows GOARCH=amd64 $(CXX) build -o dist/$(PROJECT_NAME)-windows-amd64.exe
	env GOOS=darwin GOARCH=amd64 $(CXX) build -o dist/$(PROJECT_NAME)-osx-amd64.out
	cd dist; \
	$(ZIP) $(ZIP_FLAG) $(PROJECT_NAME)-$(VERSION)-linux-386.tar $(PROJECT_NAME)-linux-386.out peers.json; \
	$(ZIP) $(ZIP_FLAG) $(PROJECT_NAME)-$(VERSION)-linux-amd64.tar $(PROJECT_NAME)-linux-amd64.out peers.json; \
	$(ZIP) $(ZIP_FLAG) $(PROJECT_NAME)-$(VERSION)-linux-arm.tar $(PROJECT_NAME)-linux-arm.out peers.json; \
	$(ZIP) $(ZIP_FLAG) $(PROJECT_NAME)-$(VERSION)-linux-arm64.tar $(PROJECT_NAME)-linux-arm64.out peers.json; \
	$(ZIP) $(ZIP_FLAG) $(PROJECT_NAME)-$(VERSION)-windows-386.tar $(PROJECT_NAME)-windows-386.exe peers.json; \
	$(ZIP) $(ZIP_FLAG) $(PROJECT_NAME)-$(VERSION)-windows-amd64.tar $(PROJECT_NAME)-windows-amd64.exe peers.json; \
	$(ZIP) $(ZIP_FLAG) $(PROJECT_NAME)-$(VERSION)-osx-amd64.tar $(PROJECT_NAME)-osx-amd64.out peers.json; \
	rm -rf peers.json *.out *.exe

