package config

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/vincenzopalazzo/SpyGoIpfs/utils"
	"testing"
)

func TestConfigOne(t *testing.T) {

	config, err := utils.LoadConfig("")

	assert.Nil(t, err)
	assert.Equal(t, len(config.Peers), 2, "Error size peer")
}
