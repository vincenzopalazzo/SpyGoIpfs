package utils

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	Peers []string
	Path  string
}

func LoadConfig(rootDir string) (*Config, error) {
	jsonFile, err := ioutil.ReadFile(rootDir + "peers.json")
	if err != nil {
		return nil, err
	}

	var config Config

	err = json.Unmarshal(jsonFile, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
