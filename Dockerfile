FROM golang:latest
LABEL mantainer="Vincenzo Palazzo vincenzopalazzodev@gmail.com"

WORKDIR work
COPY . .

RUN go get
RUN go build .