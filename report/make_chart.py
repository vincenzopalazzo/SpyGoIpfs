import json
from pyecharts.charts import Pie, Map
from pyecharts.components import Table
from pyecharts import options


def make_peers_chart(cid: str) -> Pie:
    """
    This method make the chart of the peers that work with the network
    and in a pie chart.
    """
    file_name = "{}-contributors.json".format(cid)
    with open(file_name) as file:
        data = json.load(file)
        peers = data.keys()
        rows = []
        for key in peers:
            if key not in data:
                continue
            info = data[key]
            if "Peer" in info:
                rows.append(options.PieItem(name=info["Peer"], value=info["Recv"]))
        pie = Pie()
        pie.add("Received From peers", rows)
        pie.set_series_opts(label_opts=options.LabelOpts(formatter="{b}: {c}"))
        return pie
    raise Exception("Error into find information")


def make_country_chart(cid: str) -> Pie:
    """
    This method make a pie chart to show how much the node receive from a
    determinate node.
    """
    file_name = "{}-contributors.json".format(cid)
    look_file = "{}-lookup.json".format(cid)
    with open(file_name) as file:
        with open(look_file) as lookup_file:
            data = json.load(file)
            look_file = json.load(lookup_file)
            peers = data.keys()
            rows = []
            country_value = dict()
            for key in peers:
                if key not in data or key not in look_file:
                    continue
                info = data[key]
                info_peer = look_file[key]
                country = info_peer["country"]
                if country not in country_value:
                    country_value.update({country: info["Recv"]})
                else:
                    counter_by_country = country_value[country]
                    country_value.update({country: info["Recv"] + counter_by_country})
            for key in country_value:
                rows.append(options.PieItem(name=key, value=country_value[key]))
            pie = Pie()
            pie.add("Received From peers", rows)
            pie.set_series_opts(label_opts=options.LabelOpts(formatter="{b}: {c}"))
            return pie
    raise Exception("Error into find information")


def make_country_map(cid: str) -> Map:
    """
    This method make a pie chart to show how much the node receive from a
    determinate node.
    """
    file_name = "{}-contributors.json".format(cid)
    look_file = "{}-lookup.json".format(cid)
    with open(file_name) as file:
        with open(look_file) as lookup_file:
            data = json.load(file)
            look_file = json.load(lookup_file)
            peers = data.keys()
            country_value = dict()
            for key in peers:
                if key not in data or key not in look_file:
                    continue
                info = data[key]
                info_peer = look_file[key]
                country = info_peer["country"]
                if country not in country_value:
                    country_value.update({country: info["Recv"]})
                else:
                    counter_by_country = country_value[country]
                    country_value.update({country: info["Recv"] + counter_by_country})
            countries = []
            values = []
            for key in country_value:
                countries.append(key)
                values.append(country_value[key])
            map = Map()
            map.add(
                "World Map",
                [list(z) for z in zip(countries, values)],
                "world",
                emphasis_label_opts=options.LabelOpts(is_show=False),
                emphasis_itemstyle_opts=options.ItemStyleOpts(
                    border_color="white", area_color="red"
                ),
            )
            # remove country names
            map.set_series_opts(label_opts=options.LabelOpts(is_show=False))
            map.set_global_opts(visualmap_opts=options.VisualMapOpts(max_=1100000, is_piecewise=False),
                                legend_opts=options.LegendOpts(is_show=False))
            return map
    raise Exception("Error into find information")


def make_peers_table(cid: str) -> Table:
    """
    This method take a cid and load the information from the json file
    in the local directory.

    With this data it make a chart to display the information about the peers in a table
    """
    file_name = "{}-lookup.json".format(cid)
    with open(file_name) as file:
        table = Table()
        data = json.load(file)
        peers = data.keys()
        rows = []
        header = ["CID", "continent", "country", "city", "protocol", "port"]
        for key in peers:
            info = data[key]
            # TODO bug in SpyGoIpfs I need to change Protocol to protocol
            protocol = info["Protocol"] if "Protocol" in info else "unknown"
            port = info["Port"] if "Port" in info else "unknown"
            rows.append([key, info["continent"], info["country"], info["city"], protocol, port])
        table.add(header, rows)
        return table
    raise Exception("Error into find information")
