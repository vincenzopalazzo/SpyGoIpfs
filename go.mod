module gitlab.com/vincenzopalazzo/SpyGoIpfs

go 1.15

require (
	github.com/akamensky/argparse v1.2.2
	github.com/dustin/go-humanize v1.0.0
	github.com/ipfs/go-bitswap v0.10.2
	github.com/ipfs/go-ipfs-files v0.2.0
	github.com/ipfs/go-ipld-format v0.4.0
	github.com/ipfs/interface-go-ipfs-core v0.7.0
	github.com/ipfs/kubo v0.17.0
	github.com/libp2p/go-libp2p v0.23.4
	github.com/mitchellh/mapstructure v1.1.2
	github.com/multiformats/go-multiaddr v0.7.0
	github.com/schollz/progressbar/v3 v3.7.6
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.8.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
